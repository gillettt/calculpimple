# OpenFoam benchmark

## Description
OpenFoam benchmark for GRICAD (with singularity).
The simulation is a plane channel flow with a mesh containing about 15 millions cells

## To prepare the environment

- Create the singularity container : ```./build_sig_OF```

## Install if necessary openmpi

- For example on dahu cluster with nix : ```./set_nix_profile``` 

## Modify the number of cores (by default 64), prepare the case and launch

- Modify the number in system/decomposeParDict file
- Clean the case : ```./Allclean```
- Prepare the case : ```./Allrun.pre```
- Modify the number of nodes and cores by node in the OAR script (in OAR header and in mpiexec command) or in the Allrun script for local use
- launch the simulation : ```./Allrun``` for local use  or ```oarsub -S ./launch_dahu``` for dahu cluster

## Which calculation time to compare?

In the standard output (log file or OAR.*****.stdout), keep the last value of ClockTime or ExecutionTime. Theoretically the values are very similar (if you do not use more processes than available cores)

## License
GPL v3

## Acknowledgements

[OpenFOAM](https://www.openfoam.com) is the free, open source CFD
software developed primarily by OpenCFD Ltd since 2004.
The OpenFOAM trademark is owned by OpenCFD Ltd.

